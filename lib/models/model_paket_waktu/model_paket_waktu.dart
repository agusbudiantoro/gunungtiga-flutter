class ModelPaketWaktu {
  List<ValuePaketWaktu> value;

  ModelPaketWaktu({this.value});

  ModelPaketWaktu.fromJson(Map<String, dynamic> json) {
    if (json['value'] != null) {
      value = new List<ValuePaketWaktu>();
      json['value'].forEach((v) {
        value.add(new ValuePaketWaktu.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.value != null) {
      data['value'] = this.value.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ValuePaketWaktu {
  int idPaketWaktu;
  String paketJam;
  String jam;

  ValuePaketWaktu({this.idPaketWaktu, this.paketJam, this.jam});

  ValuePaketWaktu.fromJson(Map<String, dynamic> json) {
    idPaketWaktu = json['id_paket_waktu'];
    paketJam = json['paket_jam'];
    jam = json['jam'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_paket_waktu'] = this.idPaketWaktu;
    data['paket_jam'] = this.paketJam;
    data['jam'] = this.jam;
    return data;
  }
}