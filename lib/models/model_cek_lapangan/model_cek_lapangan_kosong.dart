class ModelFormCekKetersediaanLapangan {
  String lapangan;
  String tanggalPemesanan;
  String paketWaktu;

  ModelFormCekKetersediaanLapangan(
      {this.lapangan, this.tanggalPemesanan, this.paketWaktu});

  ModelFormCekKetersediaanLapangan.fromJson(Map<String, dynamic> json) {
    lapangan = json['lapangan'];
    tanggalPemesanan = json['tanggal_pemesanan'];
    paketWaktu = json['paket_waktu'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lapangan'] = this.lapangan;
    data['tanggal_pemesanan'] = this.tanggalPemesanan;
    data['paket_waktu'] = this.paketWaktu;
    return data;
  }
}