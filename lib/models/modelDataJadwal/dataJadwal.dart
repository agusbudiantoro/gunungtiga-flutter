class dataJadwal {
  String message;
  List<ValuedataJadwal> value;

  dataJadwal({this.message, this.value});

  dataJadwal.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    if (json['value'] != null) {
      value = new List<ValuedataJadwal>();
      json['value'].forEach((v) {
        value.add(new ValuedataJadwal.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    if (this.value != null) {
      data['value'] = this.value.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ValuedataJadwal {
  int id;
  int idTim;
  String namaTim;
  String noTelp;
  String tanggalPemesanan;
  String lapangan;
  String jenisSewa;
  int hargaDp;
  String idMidtransPembayaran;
  String idMidtransTransaksi;
  String paketWaktu;
  int idMetodePembayaran;
  int idStatusPembayaran;
  int ubahStatusBayar;
  String linkPembayaran;
  String namaPembayaran;
  String logo;
  String jam;

  ValuedataJadwal(
      {this.id,
      this.idTim,
      this.namaTim,
      this.noTelp,
      this.tanggalPemesanan,
      this.lapangan,
      this.jenisSewa,
      this.hargaDp,
      this.idMidtransPembayaran,
      this.idMidtransTransaksi,
      this.paketWaktu,
      this.idMetodePembayaran,
      this.idStatusPembayaran,
      this.ubahStatusBayar,
      this.linkPembayaran,
      this.namaPembayaran,
      this.logo,
      this.jam});

  ValuedataJadwal.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idTim = json['id_tim'];
    namaTim = json['nama_tim'];
    noTelp = json['no_telp'];
    tanggalPemesanan = json['tanggal_pemesanan'];
    lapangan = json['lapangan'];
    jenisSewa = json['jenis_sewa'];
    hargaDp = json['harga_dp'];
    idMidtransPembayaran = json['id_midtrans_pembayaran'];
    idMidtransTransaksi = json['id_midtrans_transaksi'];
    paketWaktu = json['paket_waktu'];
    idMetodePembayaran = json['id_metode_pembayaran'];
    idStatusPembayaran = json['id_status_pembayaran'];
    ubahStatusBayar = json['ubah_status_bayar'];
    linkPembayaran = json['link_pembayaran'];
    namaPembayaran = json['nama_pembayaran'];
    jam = json['jam'];
    logo = json['logo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_tim'] = this.idTim;
    data['nama_tim'] = this.namaTim;
    data['no_telp'] = this.noTelp;
    data['tanggal_pemesanan'] = this.tanggalPemesanan;
    data['lapangan'] = this.lapangan;
    data['jenis_sewa'] = this.jenisSewa;
    data['harga_dp'] = this.hargaDp;
    data['id_midtrans_pembayaran'] = this.idMidtransPembayaran;
    data['id_midtrans_transaksi'] = this.idMidtransTransaksi;
    data['paket_waktu'] = this.paketWaktu;
    data['id_metode_pembayaran'] = this.idMetodePembayaran;
    data['id_status_pembayaran'] = this.idStatusPembayaran;
    data['ubah_status_bayar'] = this.ubahStatusBayar;
    data['link_pembayaran'] = this.linkPembayaran;
    data['nama_pembayaran'] = this.namaPembayaran;
    data['logo'] = this.logo;
    data['jam'] = this.jam;
    return data;
  }
}