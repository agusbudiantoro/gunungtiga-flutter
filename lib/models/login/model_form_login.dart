class MyDataFormLogin {
  String email;
  String password;
  String nama;
  String nama_tim;

  MyDataFormLogin(
      {this.email,
      this.password,
      this.nama,
      this.nama_tim
      });

  MyDataFormLogin.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
    nama = json['nama'];
    nama_tim = json['nama_tim'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['password'] = this.password;
    data['nama'] = this.nama;
    data['nama_tim'] = this.nama_tim;
    return data;
  }
}