class MyDataLogin {
  bool success;
  String message;
  String token;
  int currUser;
  int role;
  String nama;
  String email;
  String nama_tim;

  MyDataLogin(
      {this.success,
      this.message,
      this.token,
      this.currUser,
      this.role,
      this.nama,
      this.email,
      this.nama_tim
      });

  MyDataLogin.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    token = json['token'];
    currUser = json['currUser'];
    role = json['role'];
    nama = json['nama'];
    email = json['email'];
    nama_tim = json['nama_tim'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    data['token'] = this.token;
    data['currUser'] = this.currUser;
    data['role'] = this.role;
    data['nama'] = this.nama;
    data['email'] = this.email;
    data['nama_tim'] = this.nama_tim;
    return data;
  }
}