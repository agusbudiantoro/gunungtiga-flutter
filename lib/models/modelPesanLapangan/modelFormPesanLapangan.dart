class ModelFormPesanLapangan {
  int idTim;
  String namaTim;
  String noTelp;
  String tanggalPemesanan;
  String paketWaktu;
  String lapangan;
  String jenisSewa;
  int idMetodePembayaran;
  int hargaDp;

  ModelFormPesanLapangan(
      {this.idTim,
      this.namaTim,
      this.noTelp,
      this.tanggalPemesanan,
      this.paketWaktu,
      this.lapangan,
      this.jenisSewa,
      this.idMetodePembayaran,
      this.hargaDp});

  ModelFormPesanLapangan.fromJson(Map<String, dynamic> json) {
    idTim = json['id_tim'];
    namaTim = json['nama_tim'];
    noTelp = json['no_telp'];
    tanggalPemesanan = json['tanggal_pemesanan'];
    paketWaktu = json['paket_waktu'];
    lapangan = json['lapangan'];
    jenisSewa = json['jenis_sewa'];
    idMetodePembayaran = json['id_metode_pembayaran'];
    hargaDp = json['harga_dp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_tim'] = this.idTim;
    data['nama_tim'] = this.namaTim;
    data['no_telp'] = this.noTelp;
    data['tanggal_pemesanan'] = this.tanggalPemesanan;
    data['paket_waktu'] = this.paketWaktu;
    data['lapangan'] = this.lapangan;
    data['jenis_sewa'] = this.jenisSewa;
    data['id_metode_pembayaran'] = this.idMetodePembayaran;
    data['harga_dp'] = this.hargaDp;
    return data;
  }
}