import 'dart:io';

import 'package:http/http.dart' show client;
import 'package:http/io_client.dart';
import 'package:lapangan/models/model_cek_lapangan/model_cek_lapangan_kosong.dart';

const urlCekLapangan = "https://gunungtiga.qiaminimarketjaksel.com/auth/api/v2/gunungtiga/cekKetersediaanLapangan";

class CekLapanganApi {
  static Future<dynamic> cekLapangan(ModelFormCekKetersediaanLapangan data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      Map<String, String>body={
      'lapangan':data.lapangan.toString(),
      'tanggal_pemesanan':data.tanggalPemesanan.toString(),
      'paket_waktu':data.paketWaktu.toString()
    };
      print("sini");
      var response = await ioClient.post(Uri.parse(urlCekLapangan), body: body);
      print(response);
      print("cek res");
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }
}