import 'dart:io';

import 'package:http/http.dart' show client;
import 'package:http/io_client.dart';
import 'package:lapangan/models/modelPesanLapangan/modelFormPesanLapangan.dart';

const urlPostPesanan = "https://gunungtiga.qiaminimarketjaksel.com/auth/api/v2/gunungtiga/postPesanan";
const urlHapusPesanan = "https://gunungtiga.qiaminimarketjaksel.com/auth/api/v2/gunungtiga/hapusPesananById";

class PesanLapanganApi {
  static Future<dynamic> postPesanan(ModelFormPesanLapangan data, int id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    print(data);
    Map<String, String>body={
      "id_tim":id.toString(),
      "nama_tim":data.namaTim.toString(),
      "no_telp":data.noTelp.toString(),
      "tanggal_pemesanan":data.tanggalPemesanan.toString(),
      "paket_waktu":data.paketWaktu.toString(),
      "lapangan":data.lapangan.toString(),
      "jenis_sewa":data.jenisSewa.toString(),
      "id_metode_pembayaran":data.idMetodePembayaran.toString(),
      "harga_dp":data.hargaDp.toString()
    };
    try {
      print("sini");
      var response = await ioClient.post(Uri.parse(urlPostPesanan), body: body);
      print(response);
      print("cek res");
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> deletePesanan(int id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    print("masuk delete");
    Map<String, String>body={
      "id":id.toString()
    };
    try {
      print("sini");
      var response = await ioClient.delete(Uri.parse(urlHapusPesanan), body: body);
      print(response);
      print("cek res");
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }
}