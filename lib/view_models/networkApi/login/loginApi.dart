import 'dart:io';

import 'package:http/http.dart' show client;
import 'package:http/io_client.dart';
// import 'package:http/http.dart';
import 'package:lapangan/models/login/model_form_login.dart';
// import 'package:lapangan/models/login/model_login.dart';

const urlLogin = "https://gunungtiga.qiaminimarketjaksel.com/auth/api/v1/gunungtiga/login";
const urlCekToken = "https://gunungtiga.qiaminimarketjaksel.com/auth/api/v1/gunungtiga/cekOauthToken";
const urlRegister = "https://gunungtiga.qiaminimarketjaksel.com/auth/api/v1/gunungtiga/register";

class LoginApi {

  static Future<dynamic> registerAkun(data)async{
    print("masukk");
    print(data);
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    MyDataFormLogin convertData = data;
    print(convertData.email);
    Map<String, String>body={
      'email':convertData.email.toString(),
      'password':convertData.password.toString(),
      'nama':convertData.nama.toString(),
      'role':'2',
      'nama_tim':convertData.nama_tim.toString()
    };
    try {
      print("sini");
      var response = await ioClient.post(Uri.parse(urlRegister), body: body);
      print(response);
      print("cek res");
      print(response.body);
      return response.body;
    } catch (e) {
      print(e.toString());
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> cekTokenLogin(token)async{
    print("sini");
    print(token);
    HttpClient client1 = new HttpClient();
    client1.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client1);
    try {
      print("sini");
      var response = await ioClient.post(Uri.parse(urlCekToken),headers: {
          HttpHeaders.authorizationHeader: 'Bearer $token',
        },);
      print(response);
      print("cek res");
      print(response.body);
      if(response.statusCode == 200){
        return response.body;
      }else {
        return throw Exception("Data tidak ditemukan");
      }
    } catch (e) {
      print(e.toString());
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> loginAkun(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    MyDataFormLogin convertData = data;
    print(data);
    print(convertData.email);
    Map<String, String>body={
      'email':convertData.email,
      'password':convertData.password
    };
    try {
      print("sini");
      var response = await ioClient.post(Uri.parse(urlLogin), body: body);
      print(response);
      print("cek res");
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }
}