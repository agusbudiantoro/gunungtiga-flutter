import 'dart:io';

import 'package:http/http.dart' show client;
import 'package:http/io_client.dart';
import 'package:lapangan/models/modelDataJadwal/dataJadwal.dart';

const urlGetJadwal = "https://gunungtiga.qiaminimarketjaksel.com/auth/api/v2/gunungtiga/getJadwalPesananBy";
const urlGetJadwalById = "https://gunungtiga.qiaminimarketjaksel.com/auth/api/v2/gunungtiga/getJadwalPesananByIdUser";
const urlGetJadwalByStatus = "https://gunungtiga.qiaminimarketjaksel.com/auth/api/v2/gunungtiga/getPesananByStatusBayar";

class JadwalApi {
  static Future<dynamic> getJadwalByStatus(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    ValuedataJadwal convertData = data;
    print(data);
    Map<String, String>body={
      'tanggal_pemesanan':convertData.tanggalPemesanan.toString(),
      'id_status_pembayaran':convertData.idStatusPembayaran.toString()
    };
    try {
      print("sini");
      var response = await ioClient.post(Uri.parse(urlGetJadwalByStatus), body: body);
      print(response);
      print("cek res");
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> getJadwal(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    ValuedataJadwal convertData = data;
    print(data);
    Map<String, String>body={
      'tanggal_pemesanan':convertData.tanggalPemesanan.toString()
    };
    try {
      print("sini");
      var response = await ioClient.post(Uri.parse(urlGetJadwal), body: body);
      print(response);
      print("cek res");
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> getJadwalById(data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    ValuedataJadwal convertData = data;
    print(convertData.idTim);
    print("cek di api");
    Map<String, String>body={
      'tanggal_pemesanan':convertData.tanggalPemesanan.toString(),
      'id_tim':convertData.idTim.toString()
    };
    try {
      print("sini");
      var response = await ioClient.post(Uri.parse(urlGetJadwalById), body: body);
      print("cek res");
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }
}