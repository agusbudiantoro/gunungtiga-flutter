import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:lapangan/models/modelDataJadwal/dataJadwal.dart';
import 'package:lapangan/view_models/networkApi/jadwalApi/api.dart';
import 'package:meta/meta.dart';

part 'jadwal_event.dart';
part 'jadwal_state.dart';

class JadwalBloc extends Bloc<JadwalEvent, JadwalState> {
  JadwalBloc() : super(JadwalInitial());

  @override
  Stream<JadwalState> mapEventToState(
    JadwalEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if(event is EventGetJadwal){
      yield* _getJadwal(event.data);
    }
    if(event is EventGetJadwalById){
      yield* _getJadwalById(event.data);
    }
    if(event is EventGetJadwalByStatus){
      yield* _getJadwalByStatus(event.data);
    }
  }
}

Stream<JadwalState> _getJadwal(data)async*{
  yield StateGetJadwalWaiting();
  try {
    print("masuk jadwal");
    dynamic value = await JadwalApi.getJadwal(data);
    var hasil = jsonDecode(value);
    dataJadwal hasilConvert = dataJadwal.fromJson(hasil);
    List<ValuedataJadwal> hasilDataFinal = hasilConvert.value;
    yield StateGetJadwalSukses(data: hasilDataFinal);
  } catch (e) {
    yield StateGetJadwalFailed(errorMessage: e.message.toString());
  }
} 

Stream<JadwalState> _getJadwalById(data)async*{
  yield StateGetJadwalByIdWaiting();
  try {
    print("masuk jadwal");
    ValuedataJadwal myData = data;
    print(myData.tanggalPemesanan.toString()); 
    dynamic value = await JadwalApi.getJadwalById(data);
    var hasil = jsonDecode(value);
    dataJadwal hasilConvert = dataJadwal.fromJson(hasil);
    List<ValuedataJadwal> hasilDataFinal = hasilConvert.value;
    yield StateGetJadwalByIdSukses(data: hasilDataFinal);
  } catch (e) {
    yield StateGetJadwalByIdFailed(errorMessage: e.message.toString());
  }
} 

Stream<JadwalState> _getJadwalByStatus(data)async*{
  yield StateGetJadwalByStatusWaiting();
  try {
    print("masuk jadwal");
    ValuedataJadwal myData = data;
    print(myData.tanggalPemesanan.toString()); 
    dynamic value = await JadwalApi.getJadwalByStatus(data);
    var hasil = jsonDecode(value);
    dataJadwal hasilConvert = dataJadwal.fromJson(hasil);
    List<ValuedataJadwal> hasilDataFinal = hasilConvert.value;
    yield StateGetJadwalByStatusSukses(data: hasilDataFinal);
  } catch (e) {
    yield StateGetJadwalByStatusFailed(errorMessage: e.message.toString());
  }
} 

