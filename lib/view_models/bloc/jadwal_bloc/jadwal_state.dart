part of 'jadwal_bloc.dart';

@immutable
abstract class JadwalState {}

class JadwalInitial extends JadwalState {}

class StateGetJadwalSukses extends JadwalState {
  List<ValuedataJadwal> data;
  StateGetJadwalSukses({this.data});
}

class StateGetJadwalFailed extends JadwalState {
  final String errorMessage;
  StateGetJadwalFailed({this.errorMessage});
}

class StateGetJadwalWaiting extends JadwalState {
  
}

class StateGetJadwalByIdSukses extends JadwalState {
  List<ValuedataJadwal> data;
  StateGetJadwalByIdSukses({this.data});
}

class StateGetJadwalByIdFailed extends JadwalState {
  final String errorMessage;
  StateGetJadwalByIdFailed({this.errorMessage});
}

class StateGetJadwalByIdWaiting extends JadwalState {
  
}

class StateGetJadwalByStatusSukses extends JadwalState {
  List<ValuedataJadwal> data;
  StateGetJadwalByStatusSukses({this.data});
}

class StateGetJadwalByStatusFailed extends JadwalState {
  final String errorMessage;
  StateGetJadwalByStatusFailed({this.errorMessage});
}

class StateGetJadwalByStatusWaiting extends JadwalState {
  
}