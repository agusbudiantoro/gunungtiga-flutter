part of 'jadwal_bloc.dart';

@immutable
abstract class JadwalEvent {}

class EventGetJadwal extends JadwalEvent {
  ValuedataJadwal data;
  EventGetJadwal({this.data});
}

class EventGetJadwalById extends JadwalEvent {
  ValuedataJadwal data;
  EventGetJadwalById({this.data});
}

class EventGetJadwalByStatus extends JadwalEvent {
  ValuedataJadwal data;
  EventGetJadwalByStatus({this.data});
}