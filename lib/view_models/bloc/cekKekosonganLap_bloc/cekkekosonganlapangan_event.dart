part of 'cekkekosonganlapangan_bloc.dart';

@immutable
abstract class CekkekosonganlapanganEvent {}

class EventCekKekosonganLapangan extends CekkekosonganlapanganEvent {
  ModelFormCekKetersediaanLapangan data;
  EventCekKekosonganLapangan({this.data});
}