import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:lapangan/models/model_cek_lapangan/model_cek_lapangan_kosong.dart';
import 'package:lapangan/models/model_cek_lapangan/model_value_cek_lapangan.dart';
import 'package:lapangan/view_models/networkApi/cekLapanganApi/cekLapanganApi.dart';
import 'package:meta/meta.dart';

part 'cekkekosonganlapangan_event.dart';
part 'cekkekosonganlapangan_state.dart';

class CekkekosonganlapanganBloc extends Bloc<CekkekosonganlapanganEvent, CekkekosonganlapanganState> {
  CekkekosonganlapanganBloc() : super(CekkekosonganlapanganInitial());

  @override
  Stream<CekkekosonganlapanganState> mapEventToState(
    CekkekosonganlapanganEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if(event is EventCekKekosonganLapangan){
      yield* _cekLapangan(event.data);
    }
  }
}

Stream <CekkekosonganlapanganState> _cekLapangan(ModelFormCekKetersediaanLapangan data)async*{
  yield StateCekkekosonganlapanganWaiting();
  try {
    dynamic resData = await CekLapanganApi.cekLapangan(data);
    var convertData = jsonDecode(resData);
    ModelValueCekLapangan finalData = ModelValueCekLapangan.fromJson(convertData);
    yield StateCekkekosonganlapanganSukses(data: finalData);
  } catch (e) {
    yield StateCekkekosonganlapanganFailed(errorMessage: e.message.toString());
  }
}
