part of 'cekkekosonganlapangan_bloc.dart';

@immutable
abstract class CekkekosonganlapanganState {}

class CekkekosonganlapanganInitial extends CekkekosonganlapanganState {}

class StateCekkekosonganlapanganSukses extends CekkekosonganlapanganState {
  ModelValueCekLapangan data;
  StateCekkekosonganlapanganSukses({this.data});
}

class StateCekkekosonganlapanganFailed extends CekkekosonganlapanganState {
  final String errorMessage;
  StateCekkekosonganlapanganFailed({this.errorMessage});
}

class StateCekkekosonganlapanganWaiting extends CekkekosonganlapanganState {
  
}