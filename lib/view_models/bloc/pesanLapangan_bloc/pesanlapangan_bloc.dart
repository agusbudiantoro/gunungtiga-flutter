import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:lapangan/models/modalLink/link.dart';
import 'package:lapangan/models/modelPesanLapangan/modelFormPesanLapangan.dart';
import 'package:lapangan/view_models/networkApi/kirimEmail/kirimEmail.dart';
import 'package:lapangan/view_models/networkApi/pesanLapanganApi/pesanLApanganApi.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'pesanlapangan_event.dart';
part 'pesanlapangan_state.dart';

class PesanlapanganBloc extends Bloc<PesanlapanganEvent, PesanlapanganState> {
  PesanlapanganBloc() : super(PesanlapanganInitial());

  @override
  Stream<PesanlapanganState> mapEventToState(
    PesanlapanganEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if(event is EventPostPEsanLapangan){
      yield* _postPesanLapangan(event.data);
    }
    if(event is EventDeletePesanan){
      yield* _delPesanan(event.id);
    }
  }
}

Stream <PesanlapanganState> _postPesanLapangan(ModelFormPesanLapangan data)async*{
  yield StatePostPesanLapanganWaiting();
  try {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int id = preferences.getInt("myId");
    String email = await preferences.getString("myEmail");
    dynamic resData = await PesanLapanganApi.postPesanan(data, id);
    var isi = jsonDecode(resData);
    ModalLink link = ModalLink.fromJson(isi);
    dynamic valueEmail = await KirimEmailNetwork.postEmail(link.link, email);
    yield StatePostPesanLapanganSukses();
  } catch (e) {
    yield StatePostPesanLapanganFailed(errorMessage: e.message.toString());
  }
}

Stream <PesanlapanganState> _delPesanan(int id)async*{
  print("sini masuk");
  yield StateDelPesanLapanganWaiting();
  try {
    dynamic resData = await PesanLapanganApi.deletePesanan(id);
    yield StateDelPesanLapanganSukses();
  } catch (e) {
    yield StateDelPesanLapanganFailed(errorMessage: e.message.toString());
  }
}