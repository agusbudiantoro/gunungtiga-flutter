part of 'pesanlapangan_bloc.dart';

@immutable
abstract class PesanlapanganState {}

class PesanlapanganInitial extends PesanlapanganState {}

class StatePostPesanLapanganSukses extends PesanlapanganState{
  
}

class StatePostPesanLapanganFailed extends PesanlapanganState{
  final String errorMessage;
  StatePostPesanLapanganFailed({this.errorMessage});
}

class StatePostPesanLapanganWaiting extends PesanlapanganState {
  
}

class StateDelPesanLapanganSukses extends PesanlapanganState{
  
}

class StateDelPesanLapanganFailed extends PesanlapanganState{
  final String errorMessage;
  StateDelPesanLapanganFailed({this.errorMessage});
}

class StateDelPesanLapanganWaiting extends PesanlapanganState {
  
}