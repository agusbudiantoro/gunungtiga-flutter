part of 'pesanlapangan_bloc.dart';

@immutable
abstract class PesanlapanganEvent {}

class EventPostPEsanLapangan extends PesanlapanganEvent {
  ModelFormPesanLapangan data;
  EventPostPEsanLapangan({this.data});
}

class EventDeletePesanan extends PesanlapanganEvent{
  final int id;
  EventDeletePesanan({this.id});
}