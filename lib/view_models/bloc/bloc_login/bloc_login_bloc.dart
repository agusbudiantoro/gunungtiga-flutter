import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:lapangan/models/login/model_form_login.dart';
import 'package:lapangan/models/login/model_login.dart';
import 'package:lapangan/view_models/networkApi/login/loginApi.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'bloc_login_event.dart';
part 'bloc_login_state.dart';

class BlocLoginBloc extends Bloc<BlocLoginEvent, BlocLoginState> {
  BlocLoginBloc() : super(BlocLoginInitial());

  @override
  Stream<BlocLoginState> mapEventToState(
    BlocLoginEvent event,
  ) async* {
    if(event is BlocEventLogin){
      yield* _login(event.myData);
    }
    if(event is BlocCekToken){
      yield* _cekToken();
    }
    if(event is BlocRegister){
      yield* _register(event.data);
    }
  }
}

Stream<BlocLoginState> _register(data)async*{
  yield BlocStateRegistLoading();
  try {
    print("sini");
    dynamic value = await LoginApi.registerAkun(data);
    yield BlocStateRegistSukses();
  } catch (e) {
    print(e.toString());
    yield BlocaStateRegistFailed(errorMessage: e.toString());
  }
}

Stream<BlocLoginState> _cekToken()async*{
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoading();
  try {
    String token = await preferences.getString("token");
    if(token != null){
      dynamic value = await LoginApi.cekTokenLogin(token);
      print("sukses masuk");
      var hasil = jsonDecode(value);
      MyDataLogin hasilConvert = MyDataLogin.fromJson(hasil);
      print(hasilConvert.email);
      yield BlocStateCekTokenSukses(data: hasilConvert);
    } else {
      yield BlocaStateCekTokenFailed();
    }
  } catch (e) {
    print("gagal masuk");
    yield BlocaStateCekTokenFailed();
  }
}

Stream<BlocLoginState> _login(data)async*{
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield BlocStateLoading();
  try {
    print("berhasil");
    MyDataFormLogin isi = data;
    print(isi.email);
    print(isi.password);
      dynamic value = await LoginApi.loginAkun(data);
      print(value);
      var hasil = jsonDecode(value);
      MyDataLogin hasilConvert = MyDataLogin.fromJson(hasil);
      print(hasilConvert);
      print("berhasil 2");
      await preferences.setString("myName", hasilConvert.nama);
      await preferences.setString("token", hasilConvert.token);
      await preferences.setInt("myId", hasilConvert.currUser);
      await preferences.setString("myEmail", hasilConvert.email);
      await preferences.setString("myTim",hasilConvert.nama_tim);
      await preferences.setInt("myRole", hasilConvert.role);
    yield BlocStateSukses(user: isi.email, myData: hasilConvert);
  } catch (e) {
    yield BlocaStateFailed(errorMessage: e.message.toString());
  }
}