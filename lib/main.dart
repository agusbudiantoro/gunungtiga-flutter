import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lapangan/splash.dart';
import 'package:lapangan/views/pages/login.dart';
import 'package:lapangan/views/utils/colors.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'dart:async';

void main() async {
  runApp(MaterialApp(
    home: SplashScreen2(),
    debugShowCheckedModeBanner: false,
  ));
}

