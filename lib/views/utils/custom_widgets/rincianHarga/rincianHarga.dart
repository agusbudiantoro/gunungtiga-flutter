import 'package:flutter/material.dart';

class WidgetRincianHarga extends StatefulWidget {
  final double width;
  final double height;
  WidgetRincianHarga({this.height, this.width});
  @override
  _WidgetRincianHargaState createState() => _WidgetRincianHargaState();
}

class _WidgetRincianHargaState extends State<WidgetRincianHarga> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.all(15),
      height: widget.height / 8,
      width: widget.width,
      color: Colors.grey[600],
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 5),
            alignment: Alignment.centerLeft,
            child: Text(
              "Rincian Harga",
              style: TextStyle(
                  fontSize: 14,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 10),
          Container(
              padding: EdgeInsets.all(5),
              alignment: Alignment.centerLeft,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [Text("Tota Dp",style: TextStyle(
                  color: Colors.white)), Text("Rp 10.000",style: TextStyle(
                  color: Colors.white))],
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
