import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lapangan/models/modelPesanLapangan/modelFormPesanLapangan.dart';
import 'package:lapangan/view_models/bloc/pesanLapangan_bloc/pesanlapangan_bloc.dart';

import '../../colors.dart';
import 'del.dart';
typedef HapusDef = void Function();
class WidgetCard extends StatefulWidget {
  int id;
  Color colorCard;
  String field1Card;
  String isi1Card;
  String field2Card;
  String isi2Card;
  String field3Card;
  String isi3Card;
  String field4Card;
  String isi4Card;
  String field5Card;
  String isi5Card;
  String field6Card;
  String isi6Card;
  String field7Card;
  String isi7Card;
  double sizeWidth;
  double sizeHaight;
  final HapusDef callback;
  // final VoidCallback clickCallback;

  WidgetCard({this.callback,this.id,this.colorCard, this.field1Card, this.isi1Card,this.field2Card, this.isi2Card,this.field3Card, this.isi3Card,this.field4Card, this.isi4Card,this.field5Card, this.isi5Card,this.field6Card, this.isi6Card,this.field7Card, this.isi7Card, this.sizeHaight, this.sizeWidth});
  @override
  _WidgetCardState createState() => _WidgetCardState();
}

class _WidgetCardState extends State<WidgetCard> {
  PesanlapanganBloc blocHapus = PesanlapanganBloc();

  @override
  Widget build(BuildContext context) {
    return Card(
            color: widget.colorCard,
            child: Container(
              width: widget.sizeWidth,
              height: widget.sizeHaight,
              margin: EdgeInsets.all(10),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 5, right: 5, left: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(widget.field1Card+" :",style: TextStyle(color: Colors.white),),
                        Text(widget.isi1Card,style: TextStyle(color: Colors.white),)
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 5, right: 5, left: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(widget.field2Card+" :",style: TextStyle(color: Colors.white),),
                        Text(widget.isi2Card,style: TextStyle(color: Colors.white),)
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 5, right: 5, left: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(widget.field3Card+" :",style: TextStyle(color: Colors.white),),
                        Text(widget.isi3Card,style: TextStyle(color: Colors.white),)
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 5, right: 5, left: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(widget.field4Card+" :",style: TextStyle(color: Colors.white),),
                        Text(widget.isi4Card,style: TextStyle(color: Colors.white),)
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 5, right: 5, left: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(widget.field5Card+" :",style: TextStyle(color: Colors.white),),
                        Text(widget.isi5Card,style: TextStyle(color: Colors.white),)
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 5, right: 5, left: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text((widget.field6Card != null)?widget.field6Card+" :":"",style: TextStyle(color: Colors.white),),
                        Text((widget.isi6Card != null && widget.isi5Card == "member")?"Rp 15.000":(widget.isi6Card != null && widget.isi5Card == "non member")?"Rp 20.000":"",style: TextStyle(color: Colors.white),)
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 5, right: 5, left: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text((widget.field7Card != null)?widget.field7Card+" :":"",style: TextStyle(color: Colors.white),),
                        Text((widget.isi7Card != null)?widget.isi7Card:"",style: TextStyle(color: Colors.white),)
                      ],
                    ),
                  ),
                  (widget.callback != null)?Container(
                    padding: EdgeInsets.only(top: 5, right: 5, left: 5),
                    child: BlocListener<PesanlapanganBloc, PesanlapanganState>(
                      bloc: blocHapus,
                      listener: (context, state) {
                        // TODO: implement listener
                        if(state is StateDelPesanLapanganSukses){
                          print("berhasil");
                          widget.callback();
                        }
                      },
                      child: Container(
                        child: BlocBuilder<PesanlapanganBloc, PesanlapanganState>(
                          bloc: blocHapus,
                          builder: (context, state) {
                            if(state is StateDelPesanLapanganSukses){
                              return buttonHapus(context);
                            }
                            if(state is StateDelPesanLapanganWaiting){
                              Container(child:Center(child: CircularProgressIndicator(backgroundColor: gold,color: Colors.black,),));
                            }
                            if(state is StateDelPesanLapanganFailed){
                              buttonHapus(context);
                            }
                            return buttonHapus(context);
                          },
                        ),
                      ),
                    )
                  ):Container()
                ],
              ),
            ),
          );
  }

  ElevatedButton buttonHapus(BuildContext context) {
    return ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(gold),
                    ),
                    child: Text("Pembatalan Pesanan", style: TextStyle(color: Colors.black),),
                    onPressed: (){
                      return myDialog(widget.id, context);
                    },
                  );
  }

  myDialog(int id,BuildContext context){
    showDialog(
        context: context,
        builder: (BuildContext context) => CustomDialog(clickCallback: (){
          hapusBarang(id);
        },),
      );
  }

  void hapusBarang(int id){
    print(id);
    blocHapus..add(EventDeletePesanan(id: id));
  }
}