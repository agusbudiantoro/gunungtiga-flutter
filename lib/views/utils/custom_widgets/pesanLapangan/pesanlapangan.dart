import 'package:flutter/material.dart';

import '../../colors.dart';

class TexFieldCustom extends StatefulWidget {
String containerField;
TextEditingController isi = TextEditingController();
final VoidCallback tapCallBack;
final VoidCallback tapCallBackWaktu;
bool readOnly;

TexFieldCustom({this.containerField, this.isi, this.tapCallBack, this.tapCallBackWaktu, this.readOnly});
  @override
  _TexFieldCustomState createState() => _TexFieldCustomState();
}

class _TexFieldCustomState extends State<TexFieldCustom> {
  @override
  Widget build(BuildContext context) {
    return Container(
              color: Colors.grey[600],
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                        flex: 2,
                        child: TextFormField(
                          readOnly: widget.readOnly,
                          onTap: (){
                            print("call");
                            if(widget.tapCallBack != null){
                              widget.tapCallBack();
                            }
                            if(widget.tapCallBackWaktu != null){
                              widget.tapCallBackWaktu();
                            }
                          },
                          controller: widget.isi,
                          keyboardType: TextInputType.multiline,
                          minLines: 1,
                          maxLines: 5,
                          cursorColor: gold,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              hintText: widget.containerField,
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey[600])),
                              hintStyle: TextStyle(color: Colors.grey[600]),
                              labelText: widget.containerField,
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: gold),
                              ),
                              labelStyle: TextStyle(color: Colors.white)),
                        )),
                  ],
                ),
            );
  }
}