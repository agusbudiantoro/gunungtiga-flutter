import 'package:flutter/material.dart';

import '../../colors.dart';

// ignore: must_be_immutable
class TextFieldLogin extends StatefulWidget {
  final String hintText;
  TextEditingController isiField = TextEditingController();
  final IconData prefixIcon;
  final IconData suffixIcon;
  final bool isObsercure;
  final VoidCallback clickCallback;

  TextFieldLogin(
      {this.hintText,
      this.isObsercure = false,
      this.prefixIcon,
      this.suffixIcon,
      this.clickCallback,
      this.isiField});
  @override
  _TextFieldLoginState createState() => _TextFieldLoginState();
}

class _TextFieldLoginState extends State<TextFieldLogin> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: Row(
        children: [
          Icon(
            widget.prefixIcon,
            color: gold,
          ),
          SizedBox(width: 5.0),
          Expanded(
              child: TextField(
            controller: widget.isiField,
            obscureText: widget.isObsercure,
            style: TextStyle(color: gold, fontSize: 20.0),
            cursorColor: Colors.white,
            decoration: InputDecoration(
              hintText: widget.hintText,
              hintStyle:
                  TextStyle(color: gold.withOpacity(0.8), fontSize: 18.0),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: gold),
              ),
            ),
          )),
          SizedBox(width: 5),
          GestureDetector(
              onTap: () {
                print("we");
                widget.clickCallback();
              },
              child: widget.suffixIcon != null
                  ? Icon(
                      widget.suffixIcon,
                      color: gold,
                    )
                  : Container())
        ],
      ),
    );
  }
}
