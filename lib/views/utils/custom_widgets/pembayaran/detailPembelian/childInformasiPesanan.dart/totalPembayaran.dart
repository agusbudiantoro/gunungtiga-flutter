import 'package:flutter/material.dart';

class ChildWidgetTotalPesanan extends StatefulWidget {
  final String hargaBarang;
  final String jasaPengiriman;
  final double width;
  ChildWidgetTotalPesanan({this.width, this.hargaBarang, this.jasaPengiriman});
  @override
  _ChildWidgetTotalPesananState createState() =>
      _ChildWidgetTotalPesananState();
}

class _ChildWidgetTotalPesananState extends State<ChildWidgetTotalPesanan> {
  bool statusButton = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.all(5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "TOTAL",
                style: TextStyle(color: Colors.grey, fontSize: 10),
              )),
          SizedBox(
            height: 5,
          ),
          Container(
              alignment: Alignment.centerLeft,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(widget.hargaBarang.toString()),
                  TextButton(
                    child: Text("Rincian"),
                    onPressed: () {
                      setState(() {
                        statusButton = !statusButton;
                      });
                    },
                  ),
                  
                ],
              )),
              (statusButton == true)
                      ? Column(
                          children: [
                            Container(
                              // padding: EdgeInsets.only(left: 5),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Rincian Harga",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(height: 10),
                            Container(
                                padding: EdgeInsets.only(top:5),
                                alignment: Alignment.centerLeft,
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("Tota Harga Barang", style: TextStyle(color:Colors.grey),),
                                        Text("Rp 95.000",style: TextStyle(color:Colors.grey))
                                      ],
                                    ),
                                    SizedBox(height: 5),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("Ongkos Kirim",style: TextStyle(color:Colors.grey)),
                                        Text("Rp 45.000",style: TextStyle(color:Colors.grey))
                                      ],
                                    ),
                                    SizedBox(height: 5),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("Total Belanja",style: TextStyle(color:Colors.black)),
                                        Text("Rp  45.000",style: TextStyle(color:Colors.black))
                                      ],
                                    ),
                                  ],
                                ))
                          ],
                        )
                      : Container()
        ],
      ),
    );
  }
}
