import 'package:flutter/material.dart';

import 'childInformasiPesanan.dart/metodePembayaran.dart';
import 'childInformasiPesanan.dart/statusPesanan.dart';
import 'childInformasiPesanan.dart/totalPembayaran.dart';

class WidgetInformasiPesanan extends StatefulWidget {
  final double width;
  final double height;
  WidgetInformasiPesanan({this.width, this.height});
  @override
  _WidgetInformasiPesananState createState() => _WidgetInformasiPesananState();
}

class _WidgetInformasiPesananState extends State<WidgetInformasiPesanan> {
  @override
  Widget build(BuildContext context) {
    return Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.all(15),
              // height: widget.width/3.1,
              width: widget.width,
              color: Colors.white,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(left:5),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Informasi Pesanan",
                      style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                        padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                        child: Container(
                          height: 1,
                          width: widget.width,
                          color: Colors.grey.withOpacity(0.3),
                        ),
                      ),
                      ChildWidgetStatusPesanan(status: "Dibayar",waktuPembayaran: "27 Jan 2021 17:37 WIB",width: widget.width,),
                      Container(
                        padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                        child: Container(
                          height: 1,
                          width: widget.width,
                          color: Colors.grey.withOpacity(0.3),
                        ),
                      ),
                      ChildWidgetTotalPesanan(width: widget.width,hargaBarang: "Rp 90.000",jasaPengiriman: "Rp 12.000",),
                      Container(
                        padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                        child: Container(
                          height: 1,
                          width: widget.width,
                          color: Colors.grey.withOpacity(0.3),
                        ),
                      ),
                      MetodePembayaran(width: widget.width,height: widget.height,),
                ],
              ),
            );
  }
}