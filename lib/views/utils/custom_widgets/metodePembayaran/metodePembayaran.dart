import 'package:flutter/material.dart';
import 'package:lapangan/models/metodePembayaran/metodePembayaran.dart';
import 'package:lapangan/views/pages/widgets/pesanLapangan/pilihMetodePembayaran.dart';
import 'package:lapangan/views/utils/content/images.dart';
typedef MetodePembayaranDef = void Function(ValuesMetodePembayaranModel);
class WidgetMetodePembayaran extends StatefulWidget {
  final double width;
  final double height;
  final MetodePembayaranDef callback;

  WidgetMetodePembayaran({this.height, this.width, this.callback});
  @override
  _WidgetMetodePembayaranState createState() => _WidgetMetodePembayaranState();
}

class _WidgetMetodePembayaranState extends State<WidgetMetodePembayaran> {
  ValuesMetodePembayaranModel metodePembayaran = ValuesMetodePembayaranModel();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()async{
        metodePembayaran = await Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>PilihMetodePembayaran()));
        setState(() {
          metodePembayaran = metodePembayaran;
        });
        widget.callback(metodePembayaran);
      },
      child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(15),
                height: widget.height/6.5,
                width: widget.width,
                color: Colors.grey[600],
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left:5),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Metode Pembayaran",
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    (metodePembayaran != null)?new Text((metodePembayaran.idMetodePembayaran != null)?
                                        metodePembayaran.namaPembayaran:"",
                                        style: TextStyle(
                                          color: Colors.white,
                                            fontFamily: 'RobotoCondensed'),
                                        textAlign: TextAlign.left):Text("")
                                  ],
                                ),
                              ),
                              SizedBox(width: 5,),
                              Container(
                                height: 20,
                                width: widget.width/4.5,
                                // color: Colors.red,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    (metodePembayaran != null)?Container(
                                      height: 20,
                                      width: widget.width/7,
                                      child: (metodePembayaran.idMetodePembayaran != null)? new Image.network(
                                        'https://gunungtiga.qiaminimarketjaksel.com/logo/gunungtiga/'+metodePembayaran.logo.toString(),
                                        // scale: 2,
                                        fit: BoxFit.contain,
                                      ):Text(""),
                                    ):Container(),
                                    SizedBox(width: 5,),
                                    Icon(Icons.arrow_forward_ios_sharp),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                          child: Container(
                            height: 1,
                            width: widget.width,
                            color: Colors.grey.withOpacity(0.3),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
    );
  }
}