import 'package:flutter/material.dart';

const Color purpleBack = Color(0xffe8c372);
const Color blueBack = Color(0xff79d1d9);
const Color circlePurpleLight = Color(0xff43cea2);
const Color circlePurpleDark = Color(0xff185a9d);
const Color background1 = Color(0xFF80C038);

const Color gold = Color(0xFFffd700);
const Color blackBackground = Colors.black;