
// import 'package:lapangan/models/barang/model_barang.dart';
// import 'package:lapangan/models/kategori/kategoriModel.dart';
// import 'package:lapangan/models/total/model_total.dart';
// import 'package:lapangan/views/utils/content/images.dart';

// List<BarangModel> dataBarangRek(){
//   List<BarangModel> mList = [
//     BarangModel(
//       image: rekBear,
//       namaBarang: "Bear Brand",
//       harga: "Rp 5.000",
//       hargaPromo: "",
//       qty: 1,
//       deskripsi: "Semua orang pasti sudah tidak asing dengan susu yang satu ini. Susu beruang merupakan minuman yang biasa diminum ketika kamu sedang sakit. Pasalnya, manfaat susu beruang dikenal dapat membantu mempercepat proses pemulihan."
//     ),
//     BarangModel(
//       image: rekDancow,
//       namaBarang: "Dancow",
//       harga: "Rp 15.000",
//       hargaPromo: "",
//       qty: 1,
//       deskripsi: "Dancow memiliki banyak varian susu, mulai dari Dancow full cream, Dancow FortiGro, Dancow Nutritods, dan sebagainya. Jika Anda sedang mencari susu Dancow untuk buah hati, inilah artikel yang tepat. Kami akan membantu Anda memilih susu Dancow dan memberikan rekomendasi susu Dancow terbaik."
//     ),
//     BarangModel(
//       image: rekFilma,
//       namaBarang: "Filma",
//       harga: "Rp 14.500",
//       hargaPromo: "",
//       qty: 1,
//       deskripsi: "Dibuat khusus bagi para profesional yang hanya menggunakan produk-produk terbaik untuk kreasi kuliner mereka. Minyak ini memberikan rasa yang lezat dan renyah."
//     ),
//     BarangModel(
//       image: rekGulaku,
//       namaBarang: "Gulaku",
//       harga: "Rp 12.500",
//       hargaPromo: "",
//       qty: 1,
//       deskripsi: "Produk Gulaku kini bisa Anda dapatkan dalam berbagai varian. Mulai dari Gulaku Pouch, Gulaku Premium, Gulaku Sugarstick, dan Gulaku Tebu."
//     ),
//     BarangModel(
//       image: rekSosro,
//       namaBarang: "Teh Botol Sosro",
//       harga: "Rp 10.000",
//       hargaPromo: "",
//       qty: 2,
//       deskripsi: "Teh Botol Sosro 450ml isi 12 botol 1 karton (grab and gojek only) Harga merupakan untuk 1 karton"
//     ),
//   ];
//   return mList;
// }

// List<BarangModel> dataBarangPro(){
//   List<BarangModel> mList = [
//     BarangModel(
//       image: proIndomie,
//       namaBarang: "Indomie",
//       harga: "Rp 2.300",
//       hargaPromo: "RP 1.449",
//       diskon: "37%",
//       qty: 1,
//       deskripsi: "Satu bungkus Indomie standard memiliki massa 85 gram, dan terdapat 2 sachet berisi 5 bumbu-bumbuan yang disertakan, yaitu kecap manis, saus sambal, minyak palm, bubuk perasa dan bawang goreng. Indomie juga tersedia dalam versi jumbo dengan massa 120 gram. Product Variasi produk Indomie: mulai dari mie goreng, mie soup, mie regional (mie dengan variasi rasa sesuai dengan masakan tradisional daerah-daerah Indonesia), mie premium, serta mie jumboIndomie selain dapat dibeli perbungkus, dapat jugadibeli dengan paket 5 bungkus atau paket 1 kardusberisi 30 atau 40 indomie. "
//     ),
//     BarangModel(
//       image: proMilo,
//       namaBarang: "Milo",
//       harga: "Rp 2.000",
//       hargaPromo: "Rp 1.500",
//       diskon: "25%",
//       qty: 1,
//       deskripsi: "Milo adalah salah satu brand minuman di Indonesia yang terkenal dengan rasa cokelatnya yang enak. Hadir dalam berbagai varian kemasan seperti Milo Sachet, Milo Kaleng, Milo Cube, serta Es Kepal Milo yang viral. Harga produk Milo juga cukup terjangkau dan bisa diperoleh dengan mudah di mana-mana. Semua produk Milo bisa dibeli secara online di iPrice Indonesia."
//     ),
//     BarangModel(
//       image: proRegal,
//       namaBarang: "Regal Biskuit",
//       harga: "Rp 45.000",
//       hargaPromo: "Rp 27.000",
//       diskon: "40%",
//       qty: 1,
//       deskripsi: "Dibuat dari bahan-bahan pilihan terbaik menggunakan resep turun temurun dan dipanggang dengan mesin teknologi dari Jerman, menghasilkan cita rasa biskuit yang berkualitas."
//     ),
//     BarangModel(
//       image: proSariwangi,
//       namaBarang: "Sariwangi",
//       harga: "Rp 8.650",
//       hargaPromo: "Rp 5.622,5",
//       diskon: "35%",
//       qty: 1,
//       deskripsi: "SARIWANGI Teh Asli 50 sheets x 3 pcs adalah teh pilihan yang dirancang dengan teknologi modern, sehingga menghasilkan cita rasa dan aroma teh khas Indonesia. Berasal dari daun teh berkualitas yang diproses secara alami dengan cara dikeringkan (diferementasikan) tanpa menggunakan bahan pewarna atau pengawet sehingga menghasilkan warna, rasa, dan aroma teh berkualitas tinggi."
//     ),
//     BarangModel(
//       image: proSunlight,
//       namaBarang: "Sanlight",
//       harga: "Rp 13.000",
//       hargaPromo: "Rp 11.050",
//       diskon: "15%",
//       qty: 5,
//       deskripsi: "Setiap tetes Sunlight Jeruk Nipis 100 mengandung ekstrak jeruk nipis murni yang ampuh untuk membantu menghancurkan sisa lemak dan minyak dengan lebih cepat dan lebih mudah. Kini, dilengkapi dengan teknologi baru Cepat Bilas, Sunlight Jeruk Nipis lebih efektif dan 10x lebih cepat bersihkan lemak. Mampu membersihkan wadah plastik sekalipun. Dapat digunakan untuk mencuci buah dan sayur"
//     ),
//   ];
//   return mList;
// }

// List<BarangModel> dataBarangKeranjang(){
//   List<BarangModel> mList = [
//     BarangModel(
//       image: proRegal,
//       namaBarang: "Regal Biskuit",
//       harga: "Rp 45.000",
//       hargaPromo: "Rp 27.000",
//       diskon: "40%",
//       qty: 1,
//       statusCeklis: false,
//       deskripsi: "Dibuat dari bahan-bahan pilihan terbaik menggunakan resep turun temurun dan dipanggang dengan mesin teknologi dari Jerman, menghasilkan cita rasa biskuit yang berkualitas."
//     ),
//     BarangModel(
//       image: proIndomie,
//       namaBarang: "Indomie",
//       harga: "Rp 2.300",
//       hargaPromo: "RP 1.449",
//       diskon: "37%",
//       qty: 1,
//       statusCeklis: false,
//       deskripsi: "Satu bungkus Indomie standard memiliki massa 85 gram, dan terdapat 2 sachet berisi 5 bumbu-bumbuan yang disertakan, yaitu kecap manis, saus sambal, minyak palm, bubuk perasa dan bawang goreng. Indomie juga tersedia dalam versi jumbo dengan massa 120 gram. Product Variasi produk Indomie: mulai dari mie goreng, mie soup, mie regional (mie dengan variasi rasa sesuai dengan masakan tradisional daerah-daerah Indonesia), mie premium, serta mie jumboIndomie selain dapat dibeli perbungkus, dapat jugadibeli dengan paket 5 bungkus atau paket 1 kardusberisi 30 atau 40 indomie. "
//     ),
//     BarangModel(
//       image: proMilo,
//       namaBarang: "Milo",
//       harga: "Rp 2.000",
//       hargaPromo: "Rp 1.500",
//       diskon: "25%",
//       qty: 1,
//       statusCeklis: false,
//       deskripsi: "Milo adalah salah satu brand minuman di Indonesia yang terkenal dengan rasa cokelatnya yang enak. Hadir dalam berbagai varian kemasan seperti Milo Sachet, Milo Kaleng, Milo Cube, serta Es Kepal Milo yang viral. Harga produk Milo juga cukup terjangkau dan bisa diperoleh dengan mudah di mana-mana. Semua produk Milo bisa dibeli secara online di iPrice Indonesia."
//     ),
//     BarangModel(
//       image: rekBear,
//       namaBarang: "Bear Brand",
//       harga: "Rp 5.000",
//       hargaPromo: "",
//       qty: 1,
//       statusCeklis: false,
//       deskripsi: "Semua orang pasti sudah tidak asing dengan susu yang satu ini. Susu beruang merupakan minuman yang biasa diminum ketika kamu sedang sakit. Pasalnya, manfaat susu beruang dikenal dapat membantu mempercepat proses pemulihan."
//     ),
//     BarangModel(
//       image: rekDancow,
//       namaBarang: "Dancow",
//       harga: "Rp 15.000",
//       hargaPromo: "",
//       qty: 1,
//       statusCeklis: false,
//       deskripsi: "Dancow memiliki banyak varian susu, mulai dari Dancow full cream, Dancow FortiGro, Dancow Nutritods, dan sebagainya. Jika Anda sedang mencari susu Dancow untuk buah hati, inilah artikel yang tepat. Kami akan membantu Anda memilih susu Dancow dan memberikan rekomendasi susu Dancow terbaik."
//     ),
//   ];
//   return mList;
// }

// List<KategoriModel> dataKategori(){
//   List<KategoriModel> mList = [
//     KategoriModel(nama: "Minuman", id: 1),
//     KategoriModel(nama: "Makanan", id: 2),
//     KategoriModel(nama: "Bumbu dan Masakan Instan", id: 3),
//     KategoriModel(nama:"Peralatan Mandi dan Mencuci", id: 4),
//     KategoriModel(nama:"Obat-Obatan", id: 5),
//     KategoriModel(nama:"Alat Tulis", id: 6),
//     KategoriModel(nama:"Sembako", id: 7)
//   ];
//   return mList;
// }

// List<JenisTotal> jenisTotalAtHome(){
//   List<JenisTotal> mList=[
//     JenisTotal(namaTotal: "Total Pesanan Sukses",value: 20),
//     JenisTotal(namaTotal: "Total Pesanan Gagal",value: 10),
//     JenisTotal(namaTotal: "Total Pesanan Menunggu",value:5)
//   ];
//   return mList;
// }