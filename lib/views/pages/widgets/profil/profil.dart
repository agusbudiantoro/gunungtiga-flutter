import 'package:flutter/material.dart';
import 'package:lapangan/views/pages/widgets/Home/listMenu/list.dart';
import 'package:lapangan/views/utils/colors.dart';
import 'package:lapangan/views/utils/custom_widgets/login/custom_container.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../login.dart';

class PageProfil extends StatefulWidget {

  @override
  _PageProfilState createState() => _PageProfilState();
}

class _PageProfilState extends State<PageProfil> {
  String nama;
  String email;

  @override
  void initState() { 
    super.initState();
    getData();
  }

  void getData()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      nama = preferences.getString("myName");
      email = preferences.getString("myEmail");
    });
  }

  void hapusPrefer()async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.remove("myName");
      await preferences.remove("token");
      await preferences.remove("myId");
      await preferences.remove("myEmail");
  }
  
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
          width: size.width,
          height: 900,
          color: blackBackground,
          child: Stack(
            children: [
              SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                child: Container(
                    height: 1000,
                    alignment: Alignment.center,
                    child: Stack(
                      children: [
                        Positioned(
                          top: size.height / 16,
                          child: ContainerLogin(
                            width: size.width,
                            height: size.height,
                            borderRadius: 70.0,
                            child: Container(
                              alignment: Alignment.topCenter,
                              margin: EdgeInsets.only(top:size.height/10,right: 20, left: 20),
                              child: Column(
                                children: [
                                  Container(
                                    child: Text(nama.toString(), style: TextStyle(color: Colors.white, fontSize: 20),),
                                  ),
                                  SizedBox(height: 10,),
                                  Container(
                                    child: Text(email.toString(), style: TextStyle(color: Colors.white, fontSize: 14),),
                                  ),
                                  SizedBox(height:20),
                                  Container(
                                        width: size.width,
                                        child: ElevatedButton(
                                          onPressed: (){
                                            hapusPrefer();
                                            return Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>Login()), (Route<dynamic> route) => false);
                                          },
                                          child: Text("Keluar", style: TextStyle(color: Colors.black),),
                                          style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all<Color>(Colors.red)
                                          ),
                                        )
                                      )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
              ),
              Positioned(
                        top: 70,
                        left: 10,
                        child: Container(
                          child: IconButton(
                            onPressed: (){
                              Navigator.pop(context);
                            },
                            icon: Icon(Icons.arrow_back, color: Colors.white, size: 30,)
                          ),
                        )
                        ),
            ],
          )),
    );
  }
}