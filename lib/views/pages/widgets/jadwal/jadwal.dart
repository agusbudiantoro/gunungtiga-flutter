import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:lapangan/models/modelDataJadwal/dataJadwal.dart';
import 'package:lapangan/view_models/bloc/jadwal_bloc/jadwal_bloc.dart';
import 'package:lapangan/views/utils/custom_widgets/isiCard/containerGlass.dart';
import 'package:lapangan/views/utils/custom_widgets/isiCard/isiCard.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:flutter/cupertino.dart';

class PageJadwal extends StatefulWidget {

  @override
  _PageJadwalState createState() => _PageJadwalState();
}

class _PageJadwalState extends State<PageJadwal> {
  JadwalBloc bloc = JadwalBloc();
  String tanggalPemesanan;

  void initState() {
    // TODO: implement initState
    super.initState();
    var dateFormat = new DateFormat("EEE, d MMM yyyy HH:mm:ss z");
    print(DateTime.now());
    print("tgl");
    tanggalPemesanan=DateTime.now().toString().substring(0,10);
    print(tanggalPemesanan);
    bloc..add(EventGetJadwal(data: ValuedataJadwal(tanggalPemesanan: tanggalPemesanan)));
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Daftar Jadwal"),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 15),
            child: IconButton(onPressed: (){
              return DatePickerSurat();
            }, 
            icon: Icon(Icons.format_list_bulleted_sharp)))
        ],
      ),
      body: BlocBuilder<JadwalBloc, JadwalState>(
        bloc: bloc,
        builder: (context, state) {
          if(state is StateGetJadwalSukses){
            return WidgetList(size, state.data);
          }
          if(state is StateGetJadwalFailed){
            return Container(child: Center(child: Text("Gagal Menampilkan Data"),),);
          }
          if(state is StateGetJadwalWaiting){
            return Container(child: Center(child: CircularProgressIndicator(),),);
          }
          return Container();
        },
      )
    );
  }

  DatePickerSurat() async {
    DateTime newDateTime = await showRoundedDatePicker(
        listDateDisabled: [
          // DateTime.now().subtract(Duration(days: 2)),
          // DateTime.now().subtract(Duration(days: 4)),
          // DateTime.now().subtract(Duration(days: 6)),
          // DateTime.now().subtract(Duration(days: 8)),
          // DateTime.now().subtract(Duration(days: 10)),
          // DateTime.now().add(Duration(days: 2)),
          // DateTime.now().add(Duration(days: 4)),
          // DateTime.now().add(Duration(days: 6)),
          // DateTime.now().add(Duration(days: 8)),
          // DateTime.now().add(Duration(days: 10)),
        ],
        textActionButton: "Oke",
        onTapActionButton: () {
          print("klik ok");
          print(tanggalPemesanan);
          bloc..add(EventGetJadwal(data: ValuedataJadwal(tanggalPemesanan: tanggalPemesanan)));
        },
        textNegativeButton: "Close",
        textPositiveButton: "",
        context: context,
        background: Colors.white,
        theme: ThemeData.dark(),
        imageHeader: AssetImage("assets/login/imageLogin.png"),
        description: "LAPANGAN",
        onTapDay: (DateTime dateTime, bool available) {
          var df = new DateFormat("EEE, d MMM yyyy HH:mm:ss z");
          setState(() {
              tanggalPemesanan = dateTime.toString().substring(0,10);
                  print("cek tanggal");
                  print(tanggalPemesanan.toString());
            // if (statusForm == 1) {
            //   _tanggalFormSurat.text = dateTime.year.toString() +
            //       '-' +
            //       dateTime.month.toString() +
            //       '-' +
            //       dateTime.day.toString();
            // } else if (statusForm == 2) {
            //   _tanggalAwal.text = dateTime.year.toString() +
            //       '-' +
            //       dateTime.month.toString() +
            //       '-' +
            //       dateTime.day.toString();
            // } else {
            //   _tanggalAkhir.text = dateTime.year.toString() +
            //       '-' +
            //       dateTime.month.toString() +
            //       '-' +
            //       dateTime.day.toString();
            // }
          });
          if (!available) {
            showDialog(
                context: context,
                builder: (c) => CupertinoAlertDialog(
                      title: Text("This date cannot be selected."),
                      actions: <Widget>[
                        CupertinoDialogAction(
                          child: Text("OK"),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        )
                      ],
                    ));
          }
          return available;
        },
        builderDay: (DateTime dateTime, bool isCurrentDay, bool isSelected,
            TextStyle defaultTextStyle) {
          if (isSelected) {
            return Container(
              decoration: BoxDecoration(
                  color: Colors.orange[600], shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }

          if (dateTime.day == 10) {
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.pink[300], width: 4),
                  shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }
          if (dateTime.day == 12) {
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.pink[300], width: 4),
                  shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }

          return Container(
            child: Center(
              child: Text(
                dateTime.day.toString(),
                style: defaultTextStyle,
              ),
            ),
          );
        });
  }

  Container WidgetList(Size size, List<ValuedataJadwal> data) {
    return Container(
      color: Colors.black,
      child: ListView.builder(
        itemCount: data.length,
        itemBuilder: (BuildContext context, int ind){
          return ContainerGlass(
            margin: 10,
            child: WidgetCard(colorCard:Colors.transparent, isi1Card: data[ind].jam.toString(),field1Card: "Waktu Pemesanan",field2Card: "Tanggal Pemesanan",isi2Card: data[ind].tanggalPemesanan.toString(),field3Card: "Nama Tim",isi3Card: data[ind].namaTim.toString(),field4Card: "Lapangan",isi4Card: data[ind].lapangan.toString(),field5Card: "Member",isi5Card: data[ind].jenisSewa.toString(), sizeHaight: size.height/5, sizeWidth: size.width,));
        },
      ),
    );
  }
}