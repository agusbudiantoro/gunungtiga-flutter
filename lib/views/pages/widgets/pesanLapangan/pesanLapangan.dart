import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lapangan/models/metodePembayaran/metodePembayaran.dart';
import 'package:lapangan/models/modelPesanLapangan/modelFormPesanLapangan.dart';
import 'package:lapangan/models/model_cek_lapangan/model_cek_lapangan_kosong.dart';
import 'package:lapangan/models/model_paket_waktu/model_paket_waktu.dart';
import 'package:lapangan/view_models/bloc/cekKekosonganLap_bloc/cekkekosonganlapangan_bloc.dart';
import 'package:lapangan/view_models/bloc/paketwaktu_bloc/paketwaktu_bloc.dart';
import 'package:lapangan/view_models/bloc/pesanLapangan_bloc/pesanlapangan_bloc.dart';
import 'package:lapangan/views/pages/widgets/daftarPesanan/daftarPesanan.dart';
import 'package:lapangan/views/utils/colors.dart';
import 'package:lapangan/views/utils/custom_widgets/metodePembayaran/metodePembayaran.dart';
import 'package:lapangan/views/utils/custom_widgets/pesanLapangan/pesanlapangan.dart';
import 'package:lapangan/views/utils/custom_widgets/rincianHarga/rincianHarga.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PagePesan extends StatefulWidget {
  @override
  _PagePesanState createState() => _PagePesanState();
}

class _PagePesanState extends State<PagePesan> {
  PaketwaktuBloc blocPaketWaktu = PaketwaktuBloc();
  CekkekosonganlapanganBloc blocCekLapangan = CekkekosonganlapanganBloc();
  PesanlapanganBloc blocPesanLap = PesanlapanganBloc();
  final format = DateFormat("yyyy-MM-dd");
  final TextEditingController _namaTim = TextEditingController();
  TextEditingController tanggalPemesanan = TextEditingController();
  TextEditingController waktuPemesanan = TextEditingController();
  TextEditingController noTelp = TextEditingController();
  TextEditingController pilihLap = TextEditingController();
  String myValue='10.00 - 11.00';
  int idKategori=1;
  String myValueLap = 'A';
  var listLap=[
    "A",
    "B"
  ];
  String myValueMember = 'member';
  var listMember=[
    "member",
    "non member"
  ];
  int idMetodePembayaran;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
    blocPaketWaktu..add(EventGetPaketWaktu());
  }

  void myCallBack(){
    print('mycall');
    return DatePickerSurat();
  }

  myCallBackWaktu(List<ValuePaketWaktu> data){
    print('mycall2');
    return dripDownButton(data);
  }

  void getData()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
     _namaTim.text = preferences.getString("myName");
    });
  }

  cekLapangan(ModelFormCekKetersediaanLapangan data){
    print(data.lapangan);
    print(data.paketWaktu);
    print(data.tanggalPemesanan);
    print("dataku");
    blocCekLapangan..add(EventCekKekosonganLapangan(data: data));
  }

  doSomethingMetodePembayaran(ValuesMetodePembayaranModel data) { 
    setState(() {
      idMetodePembayaran=(data != null)?data.idMetodePembayaran:0;
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: false,
        actions: [
          Container(
            width: size.width,
            alignment: Alignment.centerLeft,
            child: Row(
              children: [
                IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.grey[350],
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                Center(
                  child: Text(
                    "Pesan Lapangan",
                    style: TextStyle(fontSize: 14, color: Colors.white),
                  ),
                )
              ],
            ),
          )
        ],
      ),
      body: Container(
        color: Colors.black87,
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.only(top:5),
              child: TexFieldCustom(isi: _namaTim,containerField: "Nama Tim",readOnly: false,)),
              SizedBox(
              height: 10,
            ),
            TexFieldCustom(isi: tanggalPemesanan, containerField: "Tanggal Pemesanan",tapCallBack:()=> myCallBack(), readOnly: true,),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.all(5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    BlocBuilder<PaketwaktuBloc, PaketwaktuState>(
                      bloc:blocPaketWaktu,
                      builder: (context, state) {
                        if(state is StateGetPaketWaktuSukses){
                          return dripDownButton(state.data);
                          // return TexFieldCustom(isi: waktuPemesanan, containerField: "Waktu Pemesanan",tapCallBackWaktu:()=> myCallBackWaktu(state.data),readOnly: false,);
                        }
                        if(state is StateGetPaketWaktuWaiting){
                          return Container(child:Center(child:CircularProgressIndicator()));
                        }
                        if(state is StateGetPaketWaktuFailed){
                          return Container(child:Center(child:Text("gagal ambil data")));
                        }
                        return Container();
                      },
                    ),
                    dripDownButtonLap(),
                    dripDownButtonMember()
                  ],
                ),
            ),
            SizedBox(
              height: 10,
            ),
            TexFieldCustom(isi: noTelp, containerField: "No Telp",readOnly: false,),
            SizedBox(
              height: 10,
            ),
            // TexFieldCustom(isi: pilihLap, containerField: "Pilih Lapangan",readOnly: false,),
            // SizedBox(
            //   height: 10,
            // ),
            // TexFieldCustom(isi: pilihMember, containerField: "Member/Non Member",readOnly: false,),
            // Text(_namaTim.text.toString(), style: TextStyle(color: Colors.white),),
            WidgetMetodePembayaran(height: size.height,width: size.width,callback: doSomethingMetodePembayaran,),
            SizedBox(
              height: 10,
            ),
            WidgetRincianHarga(height: size.height,width: size.width)
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(10),
        alignment: Alignment.centerLeft,
        height: size.height/9,
        color: Colors.grey[600],
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Total Pembayaran", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                Text("Rp 10.000", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold))
              ],
            ),
            BlocBuilder<CekkekosonganlapanganBloc, CekkekosonganlapanganState>(
              bloc: blocCekLapangan,
              builder: (context, state) {
                if(state is StateCekkekosonganlapanganSukses){
                  if(state.data.value == true){
                    return buttonBayar(size);
                  } else {
                    return Container(padding: EdgeInsets.only(top:10),alignment: Alignment.bottomCenter,child: Text("Lapangan Tidak Tersedia", style: TextStyle(color: Colors.red),),);
                  }
                }
                if(state is StateCekkekosonganlapanganWaiting){
                  return Center(child: CircularProgressIndicator(backgroundColor: gold,color: Colors.black,));
                }
                if(state is StateCekkekosonganlapanganFailed){
                  return Container(padding: EdgeInsets.only(top:10),alignment: Alignment.bottomCenter,child: Text("Error atau tanggal pemesanan, paket waktu dan lapangan belum dipilih", style: TextStyle(color: Colors.red),),);
                }
                return Container();
              },
            )
          ],
        ),
      ),
    );
  }

  BlocListener buttonBayar(Size size) {
    return BlocListener<PesanlapanganBloc, PesanlapanganState>(
      bloc: blocPesanLap,
      listener: (context, state) {
        // TODO: implement listener
        if(state is StatePostPesanLapanganSukses){
          Navigator.pop(context);
          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>PageDaftarPesanan()));
        }
      },
      child: Container(
        child: BlocBuilder<PesanlapanganBloc, PesanlapanganState>(
          bloc: blocPesanLap,
          builder: (context, state) {
            if(state is StatePostPesanLapanganSukses){
              return buttonPembayaran(size);
            }
            if(state is StatePostPesanLapanganWaiting){
              return Container(child:Center(child: CircularProgressIndicator(backgroundColor: gold,color: Colors.black,),));
            }
            if(state is StatePostPesanLapanganFailed){
              return buttonPembayaran(size);
            }
            return buttonPembayaran(size);
          },
        ),
      ),
    );
    
  }

  Container buttonPembayaran(Size size) {
    return Container(
                  width: size.width,
                  child: ElevatedButton(
                    onPressed: (){
                      blocPesanLap..add(EventPostPEsanLapangan(data: ModelFormPesanLapangan(namaTim: _namaTim.text.toString(),noTelp: noTelp.text.toString(),tanggalPemesanan: tanggalPemesanan.text.toString(),paketWaktu: idKategori.toString(),lapangan: myValueLap.toString(),jenisSewa: myValueMember.toString(),idMetodePembayaran: idMetodePembayaran,hargaDp: 10000)));
                    },
                    child: Text("Bayar", style: TextStyle(color: Colors.black),),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(gold)
                    ),
                  )
                );
  }

  DropdownButton<String> dripDownButtonMember() {
    return DropdownButton(
      dropdownColor: Colors.black,
      // style: TextStyle(color: Colors.white),
      value: myValueMember,
      icon: Icon(Icons.keyboard_arrow_down),
      items: listMember.map((String items) {
        return DropdownMenuItem(value: items, child: Container(color: Colors.transparent ,child: Text(items.toString(), style: TextStyle(color: Colors.white))));
      }).toList(),
      onChanged: (String newValue) {
        setState(() {
          myValueMember = newValue;
          print(myValueMember);
        });
      },
    );
  }

  DropdownButton<String> dripDownButtonLap() {
    return DropdownButton(
      dropdownColor: Colors.black,
      // style: TextStyle(color: Colors.white),
      value: myValueLap,
      icon: Icon(Icons.keyboard_arrow_down),
      items: listLap.map((String items) {
        return DropdownMenuItem(value: items, child: Container(color: Colors.transparent ,child: Text(items.toString(), style: TextStyle(color: Colors.white))));
      }).toList(),
      onChanged: (String newValue) {
        setState(() {
          myValueLap = newValue;
          print(myValueLap);
          cekLapangan(ModelFormCekKetersediaanLapangan(lapangan: myValueLap.toString(),paketWaktu: idKategori.toString(),tanggalPemesanan: tanggalPemesanan.text.toString()));
        });
      },
    );
  }

  DropdownButton<String> dripDownButton(List<ValuePaketWaktu> dataKategori) {
    List<String> isi = [];
    dataKategori.forEach((element) {
      isi.add(element.jam.toString());
    });
    return DropdownButton(
      dropdownColor: Colors.black,
      value: myValue,
      icon: Icon(Icons.keyboard_arrow_down),
      items: isi.map((String items) {
        return DropdownMenuItem(value: items, child: Container(color: Colors.transparent ,child: Text(items.toString(), style: TextStyle(color: Colors.white),)));
      }).toList(),
      onChanged: (String newValue) {
        setState(() {
          myValue = newValue;
          dataKategori.forEach((element) {
            if (element.jam == myValue) {
              idKategori = int.parse(element.paketJam);
              cekLapangan(ModelFormCekKetersediaanLapangan(paketWaktu: idKategori.toString(),lapangan: myValueLap.toString(),tanggalPemesanan: tanggalPemesanan.text.toString()));
            }
          });
        });
      },
    );
  }

  DatePickerSurat() async {
    DateTime newDateTime = await showRoundedDatePicker(
        listDateDisabled: [
          // DateTime.now().subtract(Duration(days: 2)),
          // DateTime.now().subtract(Duration(days: 4)),
          // DateTime.now().subtract(Duration(days: 6)),
          // DateTime.now().subtract(Duration(days: 8)),
          // DateTime.now().subtract(Duration(days: 10)),
          // DateTime.now().add(Duration(days: 2)),
          // DateTime.now().add(Duration(days: 4)),
          // DateTime.now().add(Duration(days: 6)),
          // DateTime.now().add(Duration(days: 8)),
          // DateTime.now().add(Duration(days: 10)),
        ],
        textActionButton: "Clear",
        onTapActionButton: () {

        },
        textPositiveButton: "Oke",
        textNegativeButton: "Close",
        context: context,
        background: Colors.white,
        theme: ThemeData.dark(),
        imageHeader: AssetImage("assets/login/imageLogin.png"),
        description: "LAPANGAN",
        onTapDay: (DateTime dateTime, bool available) {
          var df = new DateFormat("EEE, d MMM yyyy HH:mm:ss z");
          setState(() {
              tanggalPemesanan.text = dateTime.toString().substring(0,10);
                  print("cek tanggal");
                  print(tanggalPemesanan.text.toString());
                  cekLapangan(ModelFormCekKetersediaanLapangan(paketWaktu: idKategori.toString(),lapangan: myValueLap.toString(), tanggalPemesanan: tanggalPemesanan.text.toString()));
            // if (statusForm == 1) {
            //   _tanggalFormSurat.text = dateTime.year.toString() +
            //       '-' +
            //       dateTime.month.toString() +
            //       '-' +
            //       dateTime.day.toString();
            // } else if (statusForm == 2) {
            //   _tanggalAwal.text = dateTime.year.toString() +
            //       '-' +
            //       dateTime.month.toString() +
            //       '-' +
            //       dateTime.day.toString();
            // } else {
            //   _tanggalAkhir.text = dateTime.year.toString() +
            //       '-' +
            //       dateTime.month.toString() +
            //       '-' +
            //       dateTime.day.toString();
            // }
          });
          if (!available) {
            showDialog(
                context: context,
                builder: (c) => CupertinoAlertDialog(
                      title: Text("This date cannot be selected."),
                      actions: <Widget>[
                        CupertinoDialogAction(
                          child: Text("OK"),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        )
                      ],
                    ));
          }
          return available;
        },
        builderDay: (DateTime dateTime, bool isCurrentDay, bool isSelected,
            TextStyle defaultTextStyle) {
          if (isSelected) {
            return Container(
              decoration: BoxDecoration(
                  color: Colors.orange[600], shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }

          if (dateTime.day == 10) {
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.pink[300], width: 4),
                  shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }
          if (dateTime.day == 12) {
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.pink[300], width: 4),
                  shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }

          return Container(
            child: Center(
              child: Text(
                dateTime.day.toString(),
                style: defaultTextStyle,
              ),
            ),
          );
        });
  }
}
