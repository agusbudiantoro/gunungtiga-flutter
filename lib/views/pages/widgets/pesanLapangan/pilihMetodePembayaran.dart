import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lapangan/models/metodePembayaran/metodePembayaran.dart';
import 'package:lapangan/view_models/bloc/metode_pembayaran_bloc/metode_pembayaran_bloc.dart';

class PilihMetodePembayaran extends StatefulWidget {

  @override
  _PilihMetodePembayaranState createState() => _PilihMetodePembayaranState();
}

class _PilihMetodePembayaranState extends State<PilihMetodePembayaran> {
  MetodePembayaranBloc bloc = MetodePembayaranBloc();
  ValuesMetodePembayaranModel tampungData = ValuesMetodePembayaranModel();
  int _value=0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(EventGetMetodePembayaran());
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: false,
        actions: [
          Container(
            width: size.width,
            alignment: Alignment.centerLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                Center(
                  child: Text(
                    "Metode Pembayaran",
                    style: TextStyle(fontSize: 14, color: Colors.white),
                  ),
                ),
                IconButton(
                    icon: Icon(
                      Icons.send,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pop(context, tampungData);
                    }),
              ],
            ),
          )
        ],
      ),
      body: BlocBuilder<MetodePembayaranBloc, MetodePembayaranState>(
        bloc: bloc,
        builder: (context, state) {
          print(state);
          if(state is StateMetodePembayaranSukses){
            return pilihMetodePembayaran(state.data, size);
          }
          if(state is StateMetodePembayaranWaiting){
            return Container(child: Center(child: CircularProgressIndicator(),),);
          }
          if(state is StateMetodePembayaranFailed){
            return Container(child:Center(child:Text("Gagal Ambil Data")));
          }
          return Container();
        },
      ),
    );
  }

  Container pilihMetodePembayaran(List<ValuesMetodePembayaranModel> data, Size size) {
    // Color getColor(Set<MaterialState> states) {
    //   const Set<MaterialState> interactiveStates = <MaterialState>{
    //     MaterialState.pressed,
    //     MaterialState.hovered,
    //     MaterialState.focused,
    //   };
    //   if (states.any(interactiveStates.contains)) {
    //     return Colors.blue;
    //   }
    //   return Colors.black;
    // }
    return Container(
      color: Colors.black,
      child: ListView.builder(
        itemCount: data.length,
        shrinkWrap: true,
        itemBuilder: (BuildContext context,i){
          return Card(
            child: Container(
              color: Colors.grey[600],
              padding: EdgeInsets.all(5),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      alignment: Alignment.topLeft,
                      child: Radio(
                        activeColor: Colors.black,
                        value: data[i].idMetodePembayaran, 
                        groupValue: _value, 
                        onChanged: (value){
                          setState(() {
                            _value = value;
                            tampungData=data[i];
                          });
                        })
                    ),
                    Container(
                      color: Colors.grey[600],
                      height: 50,
                      width: size.width/1.5,
                      child: new Image.network(
                        'https://gunungtiga.qiaminimarketjaksel.com/logo/gunungtiga/' +
                            data[i].logo.toString(),
                        fit: BoxFit.contain,
                      ),
                    ),
                ],
              ),
            ),
          );
        }
        ),
    );
  }
}