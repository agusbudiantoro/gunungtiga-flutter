import 'package:flutter/material.dart';
import 'package:lapangan/models/login/model_form_login.dart';
import 'package:lapangan/view_models/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:lapangan/views/pages/widgets/Home/user/background.dart';
import 'package:lapangan/views/pages/widgets/home/admin/background.dart';
import 'package:lapangan/views/pages/widgets/register/register.dart';
import 'package:lapangan/views/utils/colors.dart';
import 'package:lapangan/views/utils/content/images.dart';
import 'package:lapangan/views/utils/custom_widgets/login/custom_button.dart';
import 'package:lapangan/views/utils/custom_widgets/login/custom_container.dart';
import 'package:lapangan/views/utils/custom_widgets/login/custom_textField.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class BackgroundLogin extends StatefulWidget {
  @override
  _BackgroundLoginState createState() => _BackgroundLoginState();
}

class _BackgroundLoginState extends State<BackgroundLogin> {
  bool statusPass = true;
  BlocLoginBloc bloc = BlocLoginBloc();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _passWord = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void _clickCallBack() {
    setState(() {
      statusPass = !statusPass;
      print(statusPass);
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
          child: Container(
        width: size.width,
        height: size.height,
        color: blackBackground,
        child: Stack(
          children: [
            Positioned(
              top: size.height/16,
              left: -(size.width/6),
              child: new Image.asset(picLogin,width: 550,),
            ),
            Positioned(
              bottom: 0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: ContainerLogin(
                      width: size.width,
                      height: size.height * 0.65,
                      borderRadius: 70.0,
                      child: Column(
                        children: [
                          Spacer(),
                          Text(
                            "Login Gunung Tiga",
                            style: TextStyle(
                                color: gold,
                                fontSize: 30.0,
                                fontWeight: FontWeight.bold,fontFamily: 'RobotoCondensed'),
                          ),
                          Spacer(),
                          TextFieldLogin(
                            isiField: _email,
                            hintText: "Email",
                            prefixIcon: Icons.email,
                          ),
                          TextFieldLogin(
                              isiField: _passWord,
                              hintText: "Password",
                              prefixIcon: Icons.vpn_key_rounded,
                              isObsercure: statusPass,
                              suffixIcon: Icons.remove_red_eye,
                              clickCallback: () => _clickCallBack()),
                          SizedBox(
                            height: 10.0,
                          ),
                          Container(
                            width: size.width,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                BlocListener<BlocLoginBloc, BlocLoginState>(
                                bloc: bloc,
                                listener: (context, state) {
                                  print(state);
                                  if(state is BlocStateSukses){
                                    if(state.myData.role == 1){
                                      Navigator.pop(context);
                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (context) => BackgroundHomeAdmin()));
                                    } else{
                                      Navigator.pop(context);
                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (context) => BackgroundHome()));
                                    }
                                  }
                                },
                                child: Container(
                                  child: BlocBuilder<BlocLoginBloc, BlocLoginState>(
                                    bloc: bloc,
                                    builder: (context, state) {
                                      if (state is BlocStateSukses) {
                                        return buildButtonLogin();
                                      }
                                      if (state is BlocStateLoading) {
                                        return Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                      if (state is BlocaStateFailed) {
                                        return buildButtonLogin();
                                      }
                                      if (state is BlocLoginInitial) {
                                        return buildButtonLogin();
                                      }
                                      return Container();
                                    },
                                  ),
                                ),
                              ),
                              ButtonLogin(
                              onPress: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>BackgroundRegister()));
                              },
                              buttonName: "Daftar",
                              paddingH: 35.0,
                            )
                              ],
                            ),
                          ),
                          
                          Spacer()
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      )),
    );
  }

  ButtonLogin buildButtonLogin() {
    return ButtonLogin(
      onPress: (){
        MyDataFormLogin isi = MyDataFormLogin(email: _email.text.toString(), password: _passWord.text.toString());
        bloc..add(BlocEventLogin(myData: isi));
      },
      buttonName: "Masuk",
      paddingH: 35.0,
    );
  }
}
