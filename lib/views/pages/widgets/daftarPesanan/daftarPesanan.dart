import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lapangan/models/modelDataJadwal/dataJadwal.dart';
import 'package:lapangan/view_models/bloc/jadwal_bloc/jadwal_bloc.dart';
import 'package:lapangan/views/utils/custom_widgets/isiCard/containerGlass.dart';
import 'package:lapangan/views/utils/custom_widgets/isiCard/isiCard.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:intl/intl.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageDaftarPesanan extends StatefulWidget {

  @override
  _PageDaftarPesananState createState() => _PageDaftarPesananState();
}

class _PageDaftarPesananState extends State<PageDaftarPesanan> {
  String tanggalPemesanan;
  int id_tim;

  JadwalBloc bloc = JadwalBloc();

  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }


  void getData()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      id_tim = preferences.getInt("myId");
      var dateFormat = new DateFormat("EEE, d MMM yyyy HH:mm:ss z");
      print(DateTime.now());
      print("tgl");
      tanggalPemesanan=DateTime.now().toString().substring(0,10);
    });
    await bloc..add(EventGetJadwalById(data: ValuedataJadwal(tanggalPemesanan: tanggalPemesanan,idTim: id_tim)));
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Daftar Pesanan"),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 15),
            child: IconButton(onPressed: (){
              return DatePickerSurat();
            }, 
            icon: Icon(Icons.format_list_bulleted_sharp)))
        ],
      ),
      body: BlocBuilder<JadwalBloc, JadwalState>(
        bloc: bloc,
        builder: (context, state) {
          if(state is StateGetJadwalByIdSukses){
            return WidgetList(size, state.data);
          }
          if(state is StateGetJadwalByIdFailed){
            return Container(child: Center(child: Text("Gagal Menampilkan Data"),),);
          }
          if(state is StateGetJadwalByIdWaiting){
            return Container(child: Center(child: CircularProgressIndicator(),),);
          }
          return Container();
        },
      )
    );
  }

  DatePickerSurat() async {
    DateTime newDateTime = await showRoundedDatePicker(
        listDateDisabled: [
        ],
        textActionButton: "Oke",
        onTapActionButton: () {
          print("klik ok");
          print(tanggalPemesanan);
          print(id_tim);
          bloc..add(EventGetJadwalById(data: ValuedataJadwal(tanggalPemesanan: tanggalPemesanan, idTim: id_tim)));
        },
        textNegativeButton: "Close",
        textPositiveButton: "",
        context: context,
        background: Colors.white,
        theme: ThemeData.dark(),
        imageHeader: AssetImage("assets/login/imageLogin.png"),
        description: "LAPANGAN",
        onTapDay: (DateTime dateTime, bool available) {
          var df = new DateFormat("EEE, d MMM yyyy HH:mm:ss z");
          setState(() {
              tanggalPemesanan = dateTime.toString().substring(0,10);
                  print("cek tanggal");
                  print(tanggalPemesanan.toString());
          });
          if (!available) {
            showDialog(
                context: context,
                builder: (c) => CupertinoAlertDialog(
                      title: Text("This date cannot be selected."),
                      actions: <Widget>[
                        CupertinoDialogAction(
                          child: Text("OK"),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        )
                      ],
                    ));
          }
          return available;
        },
        builderDay: (DateTime dateTime, bool isCurrentDay, bool isSelected,
            TextStyle defaultTextStyle) {
          if (isSelected) {
            return Container(
              decoration: BoxDecoration(
                  color: Colors.orange[600], shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }

          if (dateTime.day == 10) {
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.pink[300], width: 4),
                  shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }
          if (dateTime.day == 12) {
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.pink[300], width: 4),
                  shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }

          return Container(
            child: Center(
              child: Text(
                dateTime.day.toString(),
                style: defaultTextStyle,
              ),
            ),
          );
        });
  }


  Container WidgetList(Size size, List<ValuedataJadwal> data) {
    return Container(
      color: Colors.black,
      child: ListView.builder(
        itemCount: data.length,
        itemBuilder: (BuildContext context, int ind){
          return ContainerGlass(
            margin: 10,
            child: WidgetCard(callback: doSomethingMetodePembayaran,id: data[ind].id,colorCard:Colors.transparent, isi1Card: data[ind].jam.toString(),field1Card: "Waktu Pemesanan",field2Card: "Tanggal Pemesanan",isi2Card: data[ind].tanggalPemesanan.toString(),field3Card: "Nama Tim",isi3Card: data[ind].namaTim.toString(),field4Card: "Lapangan",isi4Card: data[ind].lapangan.toString(),field5Card: "Member",isi5Card: data[ind].jenisSewa.toString(), field6Card: "Sisa Pembayaran",isi6Card: "Rp 15.000", field7Card: "Status Pembayaran Dp",isi7Card: (data[ind].idStatusPembayaran == 1)?"Menunggu Pembayaran":(data[ind].idStatusPembayaran == 2)?"Dibayar":(data[ind].idStatusPembayaran == 3)?"Kadaluarsa":"", sizeHaight: size.height/3.5, sizeWidth: size.width,));
        },
      ),
    );
  }

  doSomethingMetodePembayaran() { 
    print("selesai2");
    getData();
  }
}