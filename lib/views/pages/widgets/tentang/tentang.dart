import 'package:flutter/material.dart';
import 'package:lapangan/views/pages/widgets/Home/listMenu/list.dart';
import 'package:lapangan/views/utils/colors.dart';
import 'package:lapangan/views/utils/custom_widgets/login/custom_container.dart';

class PageTentang extends StatefulWidget {

  @override
  _PageTentangState createState() => _PageTentangState();
}

class _PageTentangState extends State<PageTentang> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
          width: size.width,
          height: 900,
          color: blackBackground,
          child: Stack(
            children: [
              Container(
                height: size.height / 2,
                width: size.width,
                color: Colors.black,
                child: Container(
                  child: Column(
                    children: [
                      
                      Container(
                        height: 300,
                        color: Colors.red,
                        child: Image.asset("assets/lapangan/lapangan.jpg",fit: BoxFit.fitHeight,),
                      ),
                    ],
                  ),
                ),
              ),
              SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                child: Container(
                    height: 1000,
                    alignment: Alignment.center,
                    child: Stack(
                      children: [
                        Positioned(
                          top: size.height / 4,
                          child: ContainerLogin(
                            width: size.width,
                            height: size.height,
                            borderRadius: 70.0,
                            child: Container(
                              alignment: Alignment.topCenter,
                              margin: EdgeInsets.only(top:size.height/10,right: 20, left: 20),
                              child: Column(
                                children: [
                                  Container(
                                    child: Text("Visi", style: TextStyle(color: Colors.white, fontSize: 20),),
                                  ),
                                  SizedBox(height:20),
                                  Container(
                                    width: size.width,
                                    child: Column(
                                      children:[
                                      Text("Membentuk anak muda yang berkarakter, bermental kuat, berjiwa positif dalam suasana kebersamaan dalam rangka meraih prestasi tinggi. Menjadikan olahraga bulutangkis sebagai pelayanan olahraga yang membawa dampak buat banyak orang", style: TextStyle(color: Colors.white, fontSize: 20),)
                                    ]),
                                  ),
                                  SizedBox(height:20),
                                  Container(
                                    child: Text("Misi", style: TextStyle(color: Colors.white, fontSize: 20),),
                                  ),
                                  SizedBox(height:20),
                                  Container(
                                    width: size.width,
                                    child: Column(
                                      children:[
                                      Text("- Adanya saling mendukung di pelayanan bulutangkis", style: TextStyle(color: Colors.white, fontSize: 20),),
                                      Text("- menjadikan komunitas bulutangkis sebagai keluarga.", style: TextStyle(color: Colors.white, fontSize: 20),)
                                    ]),
                                  ),
                                  SizedBox(height:20),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.location_on, color: gold,),
                                      SizedBox(width:10),
                                      Text("Alamat",style: TextStyle(color: Colors.white, fontSize: 20),),
                                      
                                    ],
                                  ),
                                  SizedBox(height:5),
                                  Container(
                                    width: size.width,
                                    child: Column(
                                      children:[
                                      Text("Jl. Batu Ampar III No.22A, Condet, Keramat Jati, Jakarta Timur", style: TextStyle(color: Colors.white, fontSize: 18),)
                                    ]),
                                  ),
                                  SizedBox(height:15),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text("Harga 1 Jam untuk Member :",style: TextStyle(color: Colors.white, fontSize: 16),),
                                      Text("Rp 25.000", style: TextStyle(color: Colors.white, fontSize: 16),)
                                    ],
                                  ),
                                  SizedBox(height:15),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text("Harga 1 Jam untuk Non Member :",style: TextStyle(color: Colors.white, fontSize: 16),),
                                      Text("Rp 30.000", style: TextStyle(color: Colors.white, fontSize: 16),)
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
              ),
              Positioned(
                        top: 50,
                        left: 10,
                        child: Container(
                          child: IconButton(
                            onPressed: (){
                              Navigator.pop(context);
                            },
                            icon: Icon(Icons.arrow_back, color: Colors.white, size: 30,)
                          ),
                        )
                        ),
            ],
          )),
    );
  }
}