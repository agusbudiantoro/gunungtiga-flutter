import 'package:flutter/material.dart';
import 'package:lapangan/models/login/model_form_login.dart';
import 'package:lapangan/view_models/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:lapangan/views/pages/widgets/Home/listMenu/list.dart';
import 'package:lapangan/views/pages/widgets/daftarLapangan/daftarLapangan.dart';
import 'package:lapangan/views/pages/widgets/daftarPesanan/daftarPesanan.dart';
import 'package:lapangan/views/pages/widgets/jadwal/jadwal.dart';
import 'package:lapangan/views/pages/widgets/pesanLapangan/pesanLapangan.dart';
import 'package:lapangan/views/pages/widgets/profil/profil.dart';
import 'package:lapangan/views/pages/widgets/tentang/tentang.dart';
import 'package:lapangan/views/utils/colors.dart';
import 'package:lapangan/views/utils/content/images.dart';
import 'package:lapangan/views/utils/custom_widgets/login/custom_button.dart';
import 'package:lapangan/views/utils/custom_widgets/login/custom_container.dart';
import 'package:lapangan/views/utils/custom_widgets/login/custom_textField.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';



class BackgroundHome extends StatefulWidget {
  @override
  _BackgroundHomeState createState() => _BackgroundHomeState();
}

class _BackgroundHomeState extends State<BackgroundHome> {
  bool statusPass = true;
  BlocLoginBloc bloc = BlocLoginBloc();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _passWord = TextEditingController();
  String nama;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  void _clickCallBack() {
    setState(() {
      statusPass = !statusPass;
      print(statusPass);
    });
  }

  void getData()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      nama = preferences.getString("myName");
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
          width: size.width,
          height: 900,
          color: blackBackground,
          child: Stack(
            children: [
              Container(
                height: size.height / 2,
                width: size.width,
                color: Colors.black,
                child: Padding(
                  padding: EdgeInsets.only(top: size.height / 12),
                  child: Column(
                    children: [
                      Text(
                        "Selamat Datang",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'RobotoCondensed'),
                      ),
                      Text(
                        nama.toString(),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'RobotoCondensed'),
                      )
                    ],
                  ),
                ),
              ),
              SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                child: Container(
                    height: 1000,
                    alignment: Alignment.center,
                    child: Stack(
                      children: [
                        Positioned(
                          top: size.height / 4,
                          child: ContainerLogin(
                            width: size.width,
                            height: size.height,
                            borderRadius: 70.0,
                            child: Container(
                              alignment: Alignment.topCenter,
                              margin: EdgeInsets.only(top:size.height/10,right: 20, left: 20),
                              child: Wrap(
                                children: [
                                  ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.assignment_late,judul: "Tentang",fontJudul: 14, clickPage: PageTentang(),),
                                  ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.calendar_today_sharp,judul: "Jadwal",fontJudul: 14, clickPage: PageJadwal(),),
                                  ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.list_alt_rounded,judul: "Daftar Lapangan",fontJudul: 11,clickPage: PageDaftarLapangan(),),
                                  ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.book_outlined,judul: "Pesan Lapangan",fontJudul: 11, clickPage: PagePesan(),),
                                  ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.featured_play_list_rounded,judul: "Daftar Pesanan",fontJudul: 11,clickPage: PageDaftarPesanan(),),
                                  ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.assignment_ind_rounded,judul: "Profile",fontJudul: 14,clickPage: PageProfil(),),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
              ),
            ],
          )),
    );
  }

  // ButtonLogin buildButtonLogin() {
  //   return ButtonLogin(
  //     onPress: () {
  //       MyDataFormLogin isi = MyDataFormLogin(
  //           email: _email.text.toString(), password: _passWord.text.toString());
  //       bloc..add(BlocEventLogin(myData: isi));
  //     },
  //     buttonName: "Masuk",
  //     paddingH: 35.0,
  //   );
  // }
}
