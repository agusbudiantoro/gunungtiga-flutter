import 'package:flutter/material.dart';
import 'package:lapangan/views/utils/custom_widgets/isiCard/containerGlass.dart';
import 'package:lapangan/views/utils/custom_widgets/login/custom_container.dart';

class PageDaftarLapangan extends StatefulWidget {

  @override
  _PageDaftarLapanganState createState() => _PageDaftarLapanganState();
}

class _PageDaftarLapanganState extends State<PageDaftarLapangan> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Daftar Lapangan"),
      ),
      body: Container(
        color: Colors.black,
        child: Column(
          children: [
            ContainerGlass(
              width: size.width,
              height: size.height/3,
              borderRadius: 20,
              margin: 5,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.topCenter,
                    height: (size.height/3)-size.height/12,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                            'assets/lapangan/lapangan.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Container(
                    height: (size.height/3)-((size.height/3)-(size.height/12)),
                    alignment: Alignment.center,
                    child: Text("lapangan A sisi sebelah kanan dari pintu masuk", style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),),
                  )
                ],
              ),
            ),
            SizedBox(height: 10,),
            ContainerGlass(
              width: size.width,
              height: size.height/3,
              borderRadius: 20,
              margin: 5,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.topCenter,
                    height: (size.height/3)-size.height/12,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                            'assets/lapangan/lapangan2.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Container(
                    height: (size.height/3)-((size.height/3)-(size.height/12)),
                    alignment: Alignment.center,
                    child: Text("Lapangan B sisi sebelah kiri dari pintu masuk", style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}