import 'package:flutter/material.dart';
import 'package:lapangan/views/pages/widgets/login/background.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Login',
      theme: ThemeData(
        // primarySwatch: Colors.black,
        // visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BackgroundLogin(),
    );
  }
}