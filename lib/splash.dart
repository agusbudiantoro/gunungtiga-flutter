import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lapangan/view_models/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:lapangan/views/pages/login.dart';
import 'package:lapangan/views/pages/widgets/home/user/background.dart';
import 'package:lapangan/views/utils/colors.dart';

import 'views/pages/widgets/home/admin/background.dart';

class SplashScreen2 extends StatefulWidget {

  @override
  _SplashScreen2State createState() => _SplashScreen2State();
}

class _SplashScreen2State extends State<SplashScreen2> {
  BlocLoginBloc bloc = BlocLoginBloc();
  startTime() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, statuslogin);
  }

  startTimeUser() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, statustohomeUser);
  }

  startTimeAdmin() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, statustohomeAdmin);
  }

  Future<String> statuslogin() async {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Login(),
      ),
    );
  }

  Future<String> statustohomeUser() async {
    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(builder: (context) => BackgroundHome()));
    
  }

  Future<String> statustohomeAdmin() async {
    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(builder: (context) => BackgroundHomeAdmin()));
    
  }
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(BlocCekToken());
  }


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: BlocListener<BlocLoginBloc, BlocLoginState>(
          bloc: bloc,
          listener: (context, state) {
            print(state);
            // TODO: implement listener
            if(state is BlocStateCekTokenSukses){
              print("masuk");
              print(state.data.role);
              if(state.data.role == 1){
                startTimeAdmin();
              } else {
                startTimeUser();
              }
            }
            if(state is BlocaStateCekTokenFailed){
              startTime();
              print("tidak masuk");
            }
          },
          child: Container(
            child: BlocBuilder<BlocLoginBloc, BlocLoginState>(
              bloc:bloc,
              builder: (context, state) {
                if(state is BlocStateCekTokenSukses){
                  return myBody(size);
                }
                if(state is BlocStateLoading){
                  return myBody(size);
                }
                if(state is BlocaStateCekTokenFailed){
                  return myBody(size);
                }
                return myBody(size);
              },
            ),
          ),
        )
    );
  }

  Container myBody(Size size) {
    return Container(
      width: size.width,
      height: size.height,
      color: blackBackground,
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          new Image.asset("assets/logo/kok.png", width: 100),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Gunung",
                style: TextStyle(
                    fontSize: 45,
                    fontFamily: 'RobotoCondensed',
                    color: gold),
              ),
              Text("Tiga",
                  style: TextStyle(
                      fontSize: 25,
                      fontFamily: 'RobotoCondensed',
                      color: Colors.white))
            ],
          )
        ],
      ),
    );
  }
}